var BootState = {

    preload: function () {
        this.loadBoot()
    },
    loadBoot: function () {
        game.load.image('preloadBar', 'assets/images/logos-and-ux/preloadBar.png')
        game.load.image('logo', 'assets/images/logos-and-ux/logo2.png')
    },
    create: function () {
        game.state.start('PreloadState')
    }
}