var CommonState = {

    init: function () {
        stateMode = arguments[0]
        levelNumber = arguments[1]
        levelGoal = arguments[2]
        levelTimer = arguments[3]
        enemiesType = arguments[4]
        countLeaves = arguments[5]
        enemiesSpeed = arguments[6]
        enemiesCount = arguments[7]
    },

    preload: function () {
        this.scaleScreen()
        this.setBackground()
    },

    scaleScreen: function () {
        PreloadState.scaleScreen()
        game.world.setBounds(0, 0, game.width, game.height)
    },

    setBackground: function () {
        game.stage.backgroundColor = "#FFFFFF"
        if (game.height > 600) {
            background = game.add.tileSprite(0, game.world.height / 3, game.world.width, game.world.height / 3, 'background9')
        } else {
            background = game.add.tileSprite(0, game.world.height / 4, game.world.width, game.world.height / 2, 'background9')
        }
    },

    create: function () {
        this.createPhaserLogo()
        this.createLogo()
        this.createButton()
        this.createPlayerTween()
        this.createEnemiesTween()
        this.createKeyboardInput()
        this.addAudio()
    },

    createLogo: function () {
        logo = game.add.sprite(game.world.width / 4, game.world.height / 2.4, 'logo')
        logo.anchor.setTo(0.5)
        logo.scale.setTo(game.world.width / 2000 + 0.1)
    },

    createPhaserLogo: function () {
        phaserLogo = game.add.sprite(game.world.width / 1.03, game.world.centerY, stateMode + 'Logo')
        phaserLogo.anchor.setTo(1, 0.5)
        phaserLogo.scale.setTo(game.world.width / 2002.4 + 0.2)
    },

    createButton: function () {
        button = game.add.sprite(game.world.width / 4, game.world.height / 1.7, stateMode + 'Button')
        button.anchor.setTo(0.5)
        button.scale.setTo(game.world.width / 1502.4 + 0.2)
        button.inputEnabled = true
    },

    update: function () {
        this.setBackgroundMovement()
        this.setButtonLogic()
        this.updateEnemies()
    },

    updateLevel: function () {
        if (stateMode === 'next') {
            game.state.start('GameState', true, false, levelNumber + 1, levelGoal + 5,
                levelTimer + 10, enemiesType, countLeaves, enemiesSpeed + 10, enemiesCount + 3)
        } else {
            game.state.start('IntroState')
        }
    },

    changeEnemiesType: function () {
        switch (enemiesType) {
            case 'static': enemiesType = 'bouncing'
                break
            case 'bouncing': enemiesType = 'following'
                break
            default: enemiesType = 'static'
                break
        }
        return enemiesType
    },

    createPlayerTween: function () {
        player = game.add.sprite(button.x - button.width / 2.5, button.y - button.height / 2, 'player')
        player.anchor.setTo(0.5, 1)
        player.animations.add('walk-right', [5, 6, 7, 8], 10, true)
        player.animations.play('walk-right')

        playerAnimation = game.add.tween(player)
        playerAnimation.to({ x: button.x + button.width / 4 }, 4000, Phaser.Easing.Linear.None, true, 10, -1)
    },

    createEnemiesTween: function () {
        enemies = game.add.group()
        if (stateMode === 'next') {
            var enemiesType = this.changeEnemiesType(enemiesType)
            switch (enemiesType) {
                case 'static': {
                    monster = game.add.sprite(button.x + button.width / 2, button.y - button.height / 2, 'monster')
                } break
                case 'bouncing': {
                    monster = game.add.sprite(button.x + button.width / 2, button.y - button.height / 2, 'monster')
                    monsterAnimation = game.add.tween(monster)
                    monsterAnimation.to({ y: logo.y - logo.height / 4 }, 400, Phaser.Easing.Linear.None, true, 10, -1, true)
                } break
                case 'following': {
                    for (let index = 0; index < 8; index += 1) {
                        monster = enemies.create(game.rnd.integerInRange(0, game.world.width), game.rnd.integerInRange(game.world.height / 4, game.world.height / 1.5), 'monster')
                        monster.scale.setTo(0.2)
                        game.physics.arcade.enable(monster)
                        monster.animations.add('fly', [0, 1, 2, 1], 40, true)
                        monster.animations.play('fly')
                    }
                } break
            }
        } else {
            monster = game.add.sprite(button.x + button.width / 2, button.y - button.height / 2, 'monster')
        }
        monster.scale.setTo(0.2)
        monster.anchor.setTo(1)
        monster.animations.add('fly', [0, 1, 2, 1], 40, true)
        monster.animations.play('fly')
    },

    updateEnemies: function () {
        enemies.forEach(function (monster) {
            game.physics.arcade.moveToObject(monster, player, 15)
        }, this)
    },

    createKeyboardInput: function () {
        space = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR)
    },

    addAudio: function () {
        music = game.add.audio('nextLoop')
        music.play()
    },

    setBackgroundMovement: function () {
        background.tilePosition.x += 3
    },

    setButtonLogic: function () {
        if (button.input.pointerOver()) {
            button.alpha = 0.6
        } else {
            button.alpha = 1
        }

        button.events.onInputDown.add(function () {
            music = game.add.audio('button')
            music.play()
            this.updateLevel()
        }, this)

        if (space.isDown) {
            music = game.add.audio('button')
            music.play()
            this.updateLevel()
        }
    }
}