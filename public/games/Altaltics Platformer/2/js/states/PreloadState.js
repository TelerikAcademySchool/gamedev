var mapsCount = 4
var backgroundsCount = 7

var PreloadState = {

    preload: function () {
        this.createPreload()

        this.scaleScreen()

        this.loadBackgrounds()
        this.loadUE()
        this.loadAudio()
        this.loadButtons()

        this.loadMap()
        this.loadPlayer()

        this.loadBonus()
        this.loadEnemies()
    },

    createPreload: function () {
        game.stage.backgroundColor = '#FFFFFF'

        preloadBar = game.add.sprite(game.world.centerX, game.world.height / 1.5, 'preloadBar')
        preloadBar.scale.setTo(24, 2)
        preloadBar.anchor.setTo(0.5)
        game.load.setPreloadSprite(preloadBar)

        logo = game.add.sprite(game.world.width / 4, game.world.height / 2.4, 'logo')
        logo.anchor.setTo(0.5)
        logo.scale.setTo(game.world.width / 2000 + 0.1)
    },

    scaleScreen: function () {
        game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL
        game.scale.pageAlignHorizontally = true

        game.world.setBounds(0, 0, game.width, game.height)
    },

    loadBackgrounds: function () {
        for (let index = 0; index < backgroundsCount; index += 1) {
            game.load.image('background' + index, 'assets/images/backgrounds/background (' + index + ').jpg')
        }
        game.load.image('waterfall', 'assets/images/backgrounds/waterfall.png')
        game.load.image('background9', 'assets/images/backgrounds/background (9).jpg')
    },

    loadPlayer: function () {
        game.load.spritesheet('player', 'assets/images/spritesheets/dude.png', 288 / 9, 40, 9)
        game.load.spritesheet('bee', 'assets/images/spritesheets/bee.png', 160 / 2, 160 / 2, 4)
        game.load.spritesheet('portal', 'assets/images/spritesheets/portal.png', 960 / 5, 960 / 5)
        game.load.image('sphere', 'assets/images/spritesheets/sphere.png')
        game.load.image('empty', 'assets/images/spritesheets/empty.png')
    },

    loadBonus: function () {
        for (let index = 0; index < 10; index += 1) {
            game.load.image('e' + index, 'assets/images/emoticons/e (' + index + ').png')
        }
    },

    loadEnemies: function () {
        game.load.spritesheet('monster', 'assets/images/spritesheets/monster.png', 904 / 4, 176, 4)
    },

    loadUE: function () {
        game.load.image('fun', 'assets/images/logos-and-ux/fun.jpg')
        game.load.image('startLogo', 'assets/images/logos-and-ux/phaserLogo.png')
        game.load.image('nextLogo', 'assets/images/logos-and-ux/phaserRun.png')
        game.load.image('winLogo', 'assets/images/logos-and-ux/phaserRun.png')
        game.load.image('overLogo', 'assets/images/logos-and-ux/phaserRun.png')

        game.load.image('leaf', 'assets/images/logos-and-ux/leaf.png')
        game.load.image('leafRed', 'assets/images/logos-and-ux/leafRed.png')

        game.load.image('arrowLeft', 'assets/images/arrows-and-buttons/arrowLeft.png')
        game.load.image('arrowRight', 'assets/images/arrows-and-buttons/arrowRight.png')
    },

    loadAudio: function () {
        game.load.audio('bonus', ['assets/audio/bonus.mp3', 'assets/audio/bonus.ogg'])
        game.load.audio('button', ['assets/audio/button.mp3', 'assets/audio/button.ogg'])
        game.load.audio('nextLoop', ['assets/audio/nextLoop.mp3', 'assets/audio/nextLoop.ogg'])
    },

    loadButtons: function () {
        game.load.image('startButton', 'assets/images/arrows-and-buttons/startButton.png')
        game.load.image('nextButton', 'assets/images/arrows-and-buttons/nextLevelButton.png')
        game.load.image('overButton', 'assets/images/arrows-and-buttons/playAgainButton.png')
        game.load.image('winButton', 'assets/images/arrows-and-buttons/playAgainButton.png')
    },

    loadMap: function () {
        for (let index = 0; index < mapsCount; index += 1) {
            game.load.tilemap('tilemap' + index, 'assets/maps/tilemap' + index + '.json', null, Phaser.Tilemap.TILED_JSON)
        }
        game.load.image('tileset', 'assets/maps/tileset.png')
    },

    create: function () {
        game.state.start('CommonState', true, false, 'start')
    }
}