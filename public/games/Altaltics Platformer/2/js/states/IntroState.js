var IntroState = {

    preload: function () {
        this.scaleScreen()
        this.setBackground()
    },

    scaleScreen: function () {
        CommonState.scaleScreen()
        // Setting the world bounds each time when the state starts. 
        // The default bounds are the last set (which is automatically transferred from the last state).
        game.world.setBounds(0, 0, game.width, game.height)
    },

    setBackground: function () {
        // Setting white background color for this state only. By default the color is black.
        game.stage.backgroundColor = "#FFFFFF"
        background = game.add.tileSprite(0, game.world.height / 3, game.world.width, game.world.height / 3, 'background9')
    },

    create: function () {
        this.createButton()
        this.createStaticElements()

        this.createArrows()
        this.createText()
        this.createMovingElements()
        this.createKeyboardInput()
    },

    scaleAll: function (sprite, factor) {
        sprite.scale.setTo(game.world.width / factor)
    },

    createButton: function () {
        button = game.add.sprite(game.world.centerX, game.world.height * 2 / 3, 'startButton')
        button.anchor.setTo(0.5, 1)
        this.scaleAll(button, 1540)
        button.inputEnabled = true
    },

    createStaticElements: function () {

        let style = { font: "bold 64px Book Antiqua", fill: "#900C3F", align: "center" }
        leaf = game.add.sprite(0, game.height / 3, 'leaf')
        this.scaleAll(leaf, 3600)

        levelTimerText = game.add.text(game.width * 0.98, game.height / 3, '0', style)
        levelTimerText.anchor.setTo(1, 0)
        this.scaleAll(levelTimerText, 1334)

        levelText = game.add.text(0, game.height * 2 / 3, 'L. ' + 0, style)
        levelText.anchor.setTo(0, 1)
        this.scaleAll(levelText, 1334)


        goalText = game.add.text(game.width * 0.98, game.height * 2 / 3, 0, style)
        goalText.anchor.setTo(1, 1)
        this.scaleAll(goalText, 1334)
    },

    createArrows: function () {
        leafDistance = 1.3
        timerTextDistance = 2.6
        arrows = game.add.group()

        arrow = arrows.create(leaf.width * leafDistance, game.world.height / 3 + leaf.height / 2, 'arrowRight')
        arrow.anchor.setTo(0, 0.5)
        arrow = arrows.create(game.world.width - levelTimerText.width * timerTextDistance, game.world.height / 3 + levelTimerText.height / 2, 'arrowLeft')
        arrow.anchor.setTo(1, 0.5)
        arrow = arrows.create(leaf.width * leafDistance, game.world.height * 2 / 3 - levelText.height / 2, 'arrowRight')
        arrow.anchor.setTo(0, 0.5)
        arrow = arrows.create(game.world.width - levelTimerText.width * timerTextDistance, game.height * 2 / 3 - levelText.height / 2, 'arrowLeft')
        arrow.anchor.setTo(1, 0.5)

        arrowX = button.x - button.width / 3
        startY = game.height / 3 + leaf.height / 2
        difference = game.world.height / 16

        arrow = arrows.create(arrowX, startY, 'arrowRight')
        arrow.anchor.setTo(0, 0.5)
        arrow = arrows.create(arrowX, startY += difference, 'arrowRight')
        arrow.anchor.setTo(0, 0.5)
        arrow = arrows.create(arrowX, startY += difference, 'arrowRight')
        arrow.anchor.setTo(0, 0.5)
        arrow = arrows.create(arrowX, startY += difference, 'arrowRight')
        arrow.anchor.setTo(0, 0.5)

        arrows.setAll('scale.x', game.world.width / 4000)
        arrows.setAll('scale.y', game.world.width / 5334)
    },

    createText: function () {

        let style = { font: "bold 20px Book Antiqua", fill: "#900C3F", align: "center" }

        text = game.add.text(leaf.width * leafDistance + arrow.width * 1.5,
            game.world.height / 3 + leaf.height / 1.8, 'YOUR LIFE', style)
        text.anchor.setTo(0, 0.5)
        this.scaleAll(text, 1150)

        text = game.add.text(game.world.width - levelTimerText.width * timerTextDistance - arrow.width * 1.5,
            game.world.height / 3 + levelTimerText.height / 1.8, 'REMAINING TIME', style)
        text.anchor.setTo(1, 0.5)
        this.scaleAll(text, 1150)

        text = game.add.text(leaf.width * leafDistance + arrow.width * 1.5,
            game.world.height * 2 / 3 - levelTimerText.height / 2.5, 'HOW COOL YOU ARE', style)
        text.anchor.setTo(0, 0.5)
        this.scaleAll(text, 1150)

        text = game.add.text(game.world.width - levelTimerText.width * timerTextDistance - arrow.width * 1.5,
            game.world.height * 2 / 3 - levelTimerText.height / 2.5, 'MONEY.', style)
        text.anchor.setTo(1, 0.5)
        this.scaleAll(text, 1150)

        arrowX = button.x - button.width / 10
        startY = game.world.height / 3 + leaf.height / 1.8
        difference = game.world.height / 16
        smallDifference = difference / 3

        text = game.add.text(arrowX, startY, 'THE PHASER DUDE', style)
        text.anchor.setTo(0, 0.5)
        this.scaleAll(text, 1150)
        text = game.add.text(arrowX, startY + smallDifference, 'a.k.a. You', style)
        text.anchor.setTo(0, 0.5)
        this.scaleAll(text, 1150)

        text = game.add.text(arrowX, startY += difference, 'YOUR ONLY FRIEND', style)
        text.anchor.setTo(0, 0.5)
        this.scaleAll(text, 1150)

        text = game.add.text(arrowX, startY += difference, 'THE BAD GUYS', style)
        text.anchor.setTo(0, 0.5)
        this.scaleAll(text, 1150)
        text = game.add.text(arrowX, startY + smallDifference, 'nb. they evolve', style)
        text.anchor.setTo(0, 0.5)
        this.scaleAll(text, 1150)

        text = game.add.text(arrowX, startY += difference, 'THE CURRENCY', style)
        text.anchor.setTo(0, 0.5)
        this.scaleAll(text, 1150)
        text = game.add.text(arrowX, startY + smallDifference, 'in the unicorn land', style)
        text.anchor.setTo(0, 0.5)
        this.scaleAll(text, 1150)
    },

    createMovingElements: function () {

        startY = game.height / 3 + leaf.height / 2
        arrowX = button.x - button.width / 2

        player = game.add.sprite(arrowX, startY, 'player')
        player.anchor.setTo(1, 0.5)
        this.scaleAll(player, 1000)
        player.animations.add('walk-left', [5, 6, 7, 8], 10, true, 4)
        player.animations.play('walk-left')

        bee = game.add.sprite(arrowX, startY += difference, 'bee')
        bee.anchor.setTo(1, 0.5)
        bee.animations.add('fly', [0, 1, 2, 3], 40, true)
        bee.scale.setTo(0.2)
        bee.animations.play('fly')

        monster = game.add.sprite(arrowX, startY += difference, 'monster')
        monster.anchor.setTo(1, 0.5)
        this.scaleAll(monster, 5334)

        monster.animations.add('fly', [0, 1, 2, 1], 30, true)
        monster.animations.play('fly')

        emoticon = game.add.sprite(arrowX, startY += difference, 'e5')
        emoticon.anchor.setTo(1, 0.5)
        this.scaleAll(emoticon, 13334)
    },

    createKeyboardInput: function () {
        space = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR)
    },

    update: function () {
        this.setBackgroundMovement()
        this.setButtonLogic()
    },

    setBackgroundMovement: function () {
        background.tilePosition.x += 3
    },

    setButtonLogic: function () {
        if (button.input.pointerOver()) {
            button.alpha = 0.6
        } else {
            button.alpha = 1
        }

        button.events.onInputDown.add(function () {
            music = game.add.audio('button')
            music.play()
            game.state.start('GameState', true, false, 1, 5, 40, 'static', 4, 0, 20)
        }, this)

        if (space.isDown) {
            music = game.add.audio('button')
            music.play()
            game.state.start('GameState', true, false, 1, 5, 40, 'static', 4, 0, 20)
        }
    }
}