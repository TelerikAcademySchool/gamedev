let layers = []
var walkSpeed = 200
var jumpTimer = 0
var jumpSpeed = 450
var mapsCount = 4
var backgroundsCount = 7

var GameState = {
    init: function () {
        levelNumber = arguments[0]
        levelGoal = arguments[1]
        levelTimer = arguments[2]
        enemiesType = arguments[3]
        countLeaves = arguments[4]
        enemiesSpeed = arguments[5]
        enemiesCount = arguments[6]
    },
    preload: function () {
        this.setGameMode()
    },
    setGameMode: function () {
        game.physics.startSystem(Phaser.Physics.ARCADE)
        game.physics.arcade.gravity.y = 500
    },

    create: function () {
        CommonState.scaleScreen()
        this.createMap()
        this.createPortal()

        this.createPlayer()
        this.createSphere()
        this.createBee()
        this.createKeyboardKeys()

        this.createEnemies()
        this.createBonusGroup()

        this.createLives()
        this.createLevelTimer()
        this.createScreenText()
        this.createArrow()
    },

    createMap: function () {

        if (levelNumber % mapsCount === 0) {
            game.world.setBounds(0, 0, 3860, 960)
            background = game.add.tileSprite(0, 0, 3860, 960, 'background' + '0')
            map = game.add.tilemap('tilemap' + levelNumber % mapsCount)
        } else {
            game.world.setBounds(0, 0, 1800, 1600)
            background = game.add.sprite(0, 0, 'background' + levelNumber % backgroundsCount)
            background.width = game.world.width
            background.height = game.world.height
            waterfall = game.add.sprite(0, 0, 'waterfall')
            map = game.add.tilemap('tilemap' + levelNumber % mapsCount)
        }

        map.addTilesetImage('tileset', 'tileset')

        layers[3] = map.createLayer('3')
        layers[0] = map.createLayer('0')
        layers[1] = map.createLayer('1')
        layers[2] = map.createLayer('2')

        map.setCollisionByExclusion([])
    },

    createPlayer: function () {
        player = game.add.sprite(portalX, portalY, 'player')
        game.physics.arcade.enable(player)
        player.body.collideWorldBounds = true
        player.body.bounce.x = 0.2
        player.body.bounce.y = 0.2

        player.animations.add('walk-left', [0, 1, 2, 3], 10, true, 4)
        player.animations.add('walk-right', [5, 6, 7, 8], 10, true, 4)

        game.camera.follow(player)
    },

    createSphere: function () {
        sphere = game.add.sprite(portalX, portalY, 'sphere')
        sphere.scale.setTo(0.15)
        sphere.anchor.setTo(0.5)

        game.physics.arcade.enable(sphere)
        sphere.body.collideWorldBounds = true

        sphere.body.bounce.x = 0.4
        sphere.body.bounce.y = 0.4
    },

    createBee: function () {
        bee = game.add.sprite(portalX, portalY, 'bee')
        bee.animations.add('fly', [0, 1, 2, 3], 40, true)
        bee.scale.setTo(0.2)
        bee.anchor.setTo(1)
        bee.animations.play('fly')
        game.physics.arcade.enable(bee)
    },

    createEnemies: function () {
        enemies = game.add.group()
        for (let y = 0; y < enemiesCount; y += 1) {
            var randomX = game.world.randomX
            var randomY = game.world.randomY

            monster = enemies.create(randomX, randomY, 'monster')
            monster.scale.setTo(0.2)
            monster.collideWorldBounds = true
            monster.animations.add('fly', [0, 1, 2, 3], 30, true)
            monster.animations.play('fly')
            game.physics.arcade.enable(monster)
        }
    },

    createPortal: function () {
        portalX = game.rnd.integerInRange(300, game.world.width - 300)
        portalY = game.rnd.integerInRange(200, game.world.height - 200)

        portal = game.add.sprite(portalX, portalY, 'portal')
        portal.anchor.setTo(0.5)
        portal.scale.setTo(1.6)

        portal.animations.add('ready', [9, 10, 11, 12, 11, 10], 12, true)
        portal.animations.add('start', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24], 12, true)

        empty = game.add.sprite(portalX, portalY, 'empty')
        empty.anchor.setTo(0.4)
        empty.scale.setTo(0.03)
    },

    createLives: function () {
        deadLeaves = game.add.group()
        leaves = game.add.group()

        var difference = 50
        for (let index = 3 * difference; index >= 0; index -= difference) {
            deadLeave = deadLeaves.create(index, 0, 'leafRed')
        }

        deadLeaves.setAll('scale.x', 0.35)
        deadLeaves.setAll('scale.y', 0.35)
        deadLeaves.setAll('fixedToCamera', true)

        for (let index = (countLeaves - 1) * difference; index >= 0; index -= difference) {
            leave = leaves.create(index, 0, 'leaf')
        }

        leaves.setAll('scale.x', 0.35)
        leaves.setAll('scale.y', 0.35)
        leaves.setAll('fixedToCamera', true)
    },

    createKeyboardKeys: function () {
        cursors = game.input.keyboard.createCursorKeys()
        jumpButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR)
    },

    createLevelTimer: function () {
        counter = levelTimer

        let style = { font: "bold 64px Book Antiqua", fill: "#900C3F", align: "center" }
        levelTimerText = game.add.text(game.width - 30, 0, counter, style)
        levelTimerText.anchor.setTo(1, 0)
        levelTimerText.fixedToCamera = true

        game.time.events.loop(Phaser.Timer.SECOND, this.updateLevelTimer, this)
    },

    createScreenText: function () {
        let style = { font: "bold 56px Book Antiqua", fill: "#FFFFFF", align: "center" }

        levelText = game.add.text(0, game.height, 'L. ' + levelNumber, style)
        levelText.anchor.setTo(0, 1)
        levelText.fixedToCamera = true

        score = 0
        goalText = game.add.text(game.width - 30, game.height, score + ' / ' + levelGoal, style)
        goalText.anchor.setTo(1)
        goalText.fixedToCamera = true
    },

    createBonusGroup: function () {
        emoticons = game.add.group()
        this.createBonus()
    },

    createBonus: function () {
        countBonus = 20
        for (var i = 0; i < countBonus; i += 1) {
            var randomX = game.world.randomX
            var randomY = game.rnd.integerInRange(0, 1000)

            emoticon = emoticons.create(randomX, randomY, 'e' + (i % 10))

            emoticon.scale.setTo(game.rnd.integerInRange(60, 100) / 1000)
            game.physics.arcade.enable(emoticon)
            emoticon.body.collideWorldBounds = true
            emoticon.body.bounce.y = 0.20
        }
    },

    createArrow: function () {
        arrowPortal = game.add.sprite(game.width / 2, game.height - 50, 'arrowLeft')
        arrowPortal.anchor.setTo(0.5)
        arrowPortal.scale.setTo(0.5)
        arrowPortal.fixedToCamera = true
        arrowPortal.visible = 0
        game.physics.arcade.enable(arrowPortal)
    },

    update: function () {
        this.setCollisions()
        this.setMovements()

        this.setEnemiesMovenemt()
        this.setBeeMovenemt()

        this.updateCountBonus()
        this.updateLeaves()
        this.setPortalCollision()
    },

    setCollisions: function () {
        game.physics.arcade.collide(enemies, layers[0])
        game.physics.arcade.collide(enemies, emoticons)

        game.physics.arcade.collide(emoticons, layers[0])

        game.physics.arcade.collide(player, layers[0])
        game.physics.arcade.collide(player, enemies, this.setEnemyCollision())
        game.physics.arcade.collide(bee, enemies, function (_, monster) {
            monster.kill()
        })

        game.physics.arcade.collide(player, emoticons, this.setBonusCollision(), function (_, emoticon) {
            emoticon.kill()
        })
    },

    setEnemyCollision: function () {
        if (counter < levelTimer - 4 || counter > levelTimer + 4) {
            if (game.physics.arcade.collide(player, enemies)) {
                counter -= 6
                game.camera.shake(0.03, 30)
                music = game.add.audio('enemy')
                music.play()
            }
            levelTimerText.setText(counter)
            enemies.setAll('alpha', 1)
            player.alpha = 1
        } else {
            player.alpha = 0.7
            enemies.setAll('alpha', 0.2)
        }
    },

    setEnemiesMovenemt: function () {
        if (enemiesType === 'static') {
            enemies.setAll('body.bounce.x', 0.2)
            enemies.setAll('body.bounce.y', 0.2)
        } else if (enemiesType === 'bouncing') {
            enemies.setAll('body.bounce.x', 1)
            enemies.setAll('body.bounce.y', 1)
        } else if (enemiesType === 'moving') {
            enemies.forEach(function (monster) {
                monster.body.velocity.x += game.rnd.integerInRange(40, 50)
                monster.body.velocity.y += game.rnd.integerInRange(40, 50)

                monster.body.velocity.x -= game.rnd.integerInRange(40, 50)
                monster.body.velocity.y -= game.rnd.integerInRange(40, 50)
            }, this)
        } else if (enemiesType === 'following') {
            enemies.forEach(function (monster) {
                game.physics.arcade.moveToObject(monster, player, enemiesSpeed)
            }, this)
        }
    },

    setBeeMovenemt: function () {
        var beeSpeed = 35
        var minBeeTime = 600
        bee.rotation = game.physics.arcade.moveToObject(bee, player, beeSpeed, minBeeTime)
    },

    setBonusCollision: function () {
        if (game.physics.arcade.overlap(player, emoticons)) {
            score += 1
            goalText.setText(score + ' / ' + levelGoal)

            counter += 4
            levelTimerText.setText(counter)

            music = game.add.audio('bonus')
            music.play()
        }
    },

    setPortalCollision: function () {
        if (score >= levelGoal) {
            arrowPortal.visible = 1
            arrowPortal.rotation = game.physics.arcade.moveToObject(arrowPortal, portal, 0, 3000)

            let style = { font: "bold 56px Book Antiqua", fill: "#900C3F", align: "center" }
            goalText.setStyle(style)

            portal.animations.play('ready')
            game.physics.enable(empty)
            empty.body.moves = false

            this.updateLevel()
        } else {
            portal.animations.play('start')
        }
    },

    setMovements: function () {
        sphere.body.x = player.body.x - 30
        sphere.body.y = player.body.y - 40

        sphereAngle = 5

        player.body.velocity.x = 0

        if (jumpButton.isDown && player.body.onFloor() && game.time.now > jumpTimer) {
            player.body.velocity.y -= jumpSpeed
            jumpTimer = game.time.now + 350
        }
        if (cursors.up.isDown) {
            player.body.velocity.y = -walkSpeed
        }
        if (cursors.left.isDown) {
            player.animations.play('walk-left')
            player.body.velocity.x = -walkSpeed
            sphere.angle -= sphereAngle
        } else if (cursors.right.isDown) {
            player.animations.play('walk-right')
            player.body.velocity.x = walkSpeed
            sphere.angle += sphereAngle
        } else {
            player.animations.stop()
            player.frame = 4
        }
    },

    updateLeaves: function () {

        if (counter <= 0) {
            if (leaves.countLiving() > 1) {
                leaves.getFirstAlive().kill()
                counter += levelTimer
            } else {
                game.state.start('CommonState', true, false, 'over')
            }
        }
    },

    updateLevel: function () {
        if (game.physics.arcade.collide(player, empty)) {
            lastLevel = 100
            if (levelNumber === lastLevel) {
                game.state.start('CommonState', true, false, 'win')
            } else {
                game.state.start('CommonState', true, false, 'next', levelNumber, levelGoal, levelTimer,
                    enemiesType, Phaser.Math.min(leaves.countLiving() + Phaser.Math.floorTo(counter / 50), 6), enemiesSpeed, enemiesCount)
            }
        }
    },

    updateLevelTimer: function () {
        levelTimerText.setText(counter)
        counter -= 1
    },

    updateCountBonus: function () {
        if (emoticons.countLiving() < 10) {
            this.createBonus()
        }
    }
}