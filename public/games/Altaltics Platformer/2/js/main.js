var game = new Phaser.Game(1200, 800, Phaser.AUTO)

game.state.add('BootState', BootState)
game.state.add('PreloadState', PreloadState)
game.state.add('IntroState', IntroState)
game.state.add('CommonState', CommonState)
game.state.add('GameState', GameState)

game.state.start('BootState')