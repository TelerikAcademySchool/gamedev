var game = new Phaser.Game(800, 800, Phaser.AUTO, '', { preload: myPreload, create: myCreate, update: myUpdate })

let fishCountBegin = 100
let fishCount = fishCountBegin
let score = 0
let mode = 'normal'
let randomColor = Phaser.Color.getRandomColor(0, 255, 10)

function myPreload() {
  scaleMode()
  loadAssets()
}

function scaleMode() {
  game.scale.pageAlignHorizontally = true
}

function loadAssets() {
  game.load.tilemap('tilemap', 'assets/maps/tilemap.json', null, Phaser.Tilemap.TILED_JSON)
  game.load.image('tileset', 'assets/maps/tileset.png')

  game.load.spritesheet('mosquito', 'assets/images/mosquito.png', 1024 / 3, 675 / 2, 5)

  for (let index = 0; index < 7; index += 1) {
    game.load.image('fish' + index, 'assets/images/fish/fish (' + index + ').png')
  }

  game.load.image('blood', 'assets/images/blood.png')

  game.load.image('waterButton', 'assets/images/buttons/waterButton.png')
  game.load.image('normalButton', 'assets/images/buttons/normalButton.png')

  game.load.audio('fish', ['assets/audio/fish.mp3', 'assets/audio/fish.ogg'])
}

function myCreate() {
  createMap()
  createPlayer()
  createFish()
  createBlood()
  createButtons()
  createText()

  addMusic()

  cursors = game.input.keyboard.createCursorKeys()
}

function createMap() {
  game.world.setBounds(0, 0, 3000, 3000)

  map = game.add.tilemap('tilemap')
  map.addTilesetImage('tileset', 'tileset')

  groundLayer = map.createLayer('0')
  groundLayer.alpha = 0.8
  detailsLayer = map.createLayer('1')
  detailsLayer.alpha = 0.3
  bonusLayer = map.createLayer('2')
  bonusLayer.alpha = 0.3
  arrowsLayer = map.createLayer('3')

  map.setCollisionByExclusion([])
}

function createPlayer() {
  player = game.add.sprite(0, 30, 'mosquito')
  player.scale.setTo(0.16)

  player.animations.add('fly', [0, 1, 2, 4, 3, 4, 2, 1], 50, true)

  game.physics.arcade.enable(player)
  player.body.collideWorldBounds = true

  game.camera.follow(player)
}

function createFish() {
  fishPool = game.add.group()

  for (var i = 0; i < fishCountBegin; i += 1) {

    var fishBorderX = game.rnd.integerInRange(0, game.world.width / 2)
    var fishBorderY = game.rnd.integerInRange(0, game.world.width / 2)
    fish = fishPool.create(fishBorderX, fishBorderY, 'fish' + i % 6, 0)

    fish.scale.setTo(game.rnd.integerInRange(250, 600) / 1000)

    game.physics.arcade.enable(fish)
    fish.body.collideWorldBounds = true
  }
}

function createBlood() {
  blood = game.add.sprite(game.world.centerX + 150, game.world.centerY + 400, 'blood')
  blood.scale.setTo(0.6)
  game.physics.arcade.enable(blood)
}

function createButtons() {
  waterButton = game.add.sprite(game.width * 0.01, game.height * 0.75, 'waterButton')
  waterButton.fixedToCamera = true
  waterButton.inputEnabled = true

  normalButton = game.add.sprite(game.width * 0.01, game.height * 0.83, 'normalButton')
  normalButton.fixedToCamera = true
  normalButton.inputEnabled = true
}

function createText() {
  let style = { font: "normal 700 65px Ubuntu sans-serif", fill: "#000000" }
  text = game.add.text(game.width * 0.01, game.height * 0.91, "Score:", style)
  text.fixedToCamera = true
}

function addMusic() {
  music = game.add.audio('fish')
  music.play()
}

function myUpdate() {
  buttonsLogic()
  changeMode()
  playerMovements()
  fishMovements()
  fishCollision()
  bloodCollision()
}

function buttonsLogic() {
  waterButton.events.onInputDown.add(function () {
    randomColor = Phaser.Color.getRandomColor(0, 255, 10)
    mode = 'water'
  })

  normalButton.events.onInputDown.add(function () {
    mode = 'normal'
  })
}

function changeMode() {
  if (mode === 'water') {
    player.scale.setTo(0.4)
    groundLayer.alpha = 0
    detailsLayer.alpha = 0
    bonusLayer.alpha = 0
    arrowsLayer.alpha = 0

    game.stage.backgroundColor = randomColor
  } else {
    player.scale.setTo(0.2)
    game.stage.backgroundColor = '#FFFFFF'

    groundLayer.alpha = 0.8
    detailsLayer.alpha = 0.3
    bonusLayer.alpha = 0.3

    game.physics.arcade.collide(player, groundLayer)
    game.physics.arcade.collide(fishPool, groundLayer)
  }
}

function playerMovements() {
  let playerSpeed = 500

  if (cursors.up.isDown) {
    player.animations.play('fly')
    player.body.velocity.y = -playerSpeed
  } else if (cursors.down.isDown) {
    player.animations.play('fly')
    player.body.velocity.y = playerSpeed
  } else if (cursors.left.isDown) {
    player.animations.play('fly')
    player.body.velocity.x = -playerSpeed
  } else if (cursors.right.isDown) {
    player.animations.play('fly')
    player.body.velocity.x = playerSpeed
  } else {
    player.body.velocity.x = 0
    player.body.velocity.y = 0
    player.animations.stop()
  }
}

function fishMovements() {
  let fishSpeed = 50

  fishPool.forEach(function (fish) {
    fish.body.velocity.x += game.rnd.integerInRange(0, fishSpeed)
    fish.body.velocity.y += game.rnd.integerInRange(0, fishSpeed)
    fish.body.velocity.x -= game.rnd.integerInRange(0, fishSpeed)
    fish.body.velocity.y -= game.rnd.integerInRange(0, fishSpeed)
  })
}

function fishCollision() {
  game.physics.arcade.collide(player, fishPool, function (_, fish) {
    text.setText("Score: " + score)

    fish.kill()
    fishCount -= 1
    score += 1
  })
}

function bloodCollision() {
  game.physics.arcade.collide(player, blood, function () {
    score = 0
    fishCount = fishCountBegin
    game.state.start(game.state.current)
  })
}