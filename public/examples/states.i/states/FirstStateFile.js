'use strict'
// Създаване на обект за първото състояние
const FirstStateVariable = {
    // Метод на това състояние
    preload: function () {
        // Зареждане на всички ресурси
        for (let index = 1; index <= 3; index += 1) {
            Game.load.image('background' + index, '../assets/images/background-sky (' + index + ').png')
        }

    },
    // Метод на това състояние
    create: function () {
        // Използване на метод на това състояния. Същото може да се напише и като FirstStateVariable.createBackground(1)
        this.createBackground(1)
        console.log("You are in the First State.")

        Game.input.onDown.add(function () {
            alert("You are going to the Second State.")
            // Преминаване към следващото състояние
            Game.state.start("SecondStateName")
        })
    },
    // Метод на това състояние, който се използва и в другите
    createBackground(imageNumber) {
        const background = Game.add.sprite(0, 0, 'background' + imageNumber)
        background.width = Game.width
        background.height = Game.height
    }
}