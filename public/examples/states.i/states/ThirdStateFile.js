'use strict'
// Създаване на обект за третото състояние
const ThirdStateVariable = {
    // Метод на това състояние
    create: function () {
        // Преизползване на метод от първото състояние
        FirstStateVariable.createBackground(3)
        console.log("You are in the Third State.")

        Game.input.onDown.add(function () {
            alert("There are no more states.")
        })
    }
}