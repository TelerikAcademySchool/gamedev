'use strict'
// Създаване на обект за второто състояние
const SecondStateVariable = {
    // Метод на това състояние
    create: function () {
        // Преизползване на метод от първото състояние
        FirstStateVariable.createBackground(2)
        console.log("You are in the Second State.")

        Game.input.onDown.add(function () {
            alert("You are going to the Third State.")
            // Преминаване към следващото състояние
            Game.state.start("ThirdStateName")
        })
    }
}