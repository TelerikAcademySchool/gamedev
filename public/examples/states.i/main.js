'use strict'
// Създаване на игра
const Game = new Phaser.Game(600, 400, Phaser.AUTO, 'game-canvas')

// Добавяне на всички състояния с ключ и името на обекта (променливата), в който са запазени
Game.state.add('FirstStateName', FirstStateVariable)
Game.state.add('SecondStateName', SecondStateVariable)
Game.state.add('ThirdStateName', ThirdStateVariable)

// Стартиране на първото състояние
Game.state.start('FirstStateName')