'use strict'
const Game = new Phaser.Game(600, 600, Phaser.AUTO, 'game-canvas')

Game.state.add('preload', Preload)
Game.state.add('functions', Functions)
Game.state.add('first', FirstState)
Game.state.add('second', SecondState)
Game.state.add('third', ThirdState)

Game.state.start('preload')