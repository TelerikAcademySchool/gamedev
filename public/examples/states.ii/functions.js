'use strict'
const Functions = {
    createBackgound: function (_number) {
        const BACKGROUND = Game.add.sprite(0, 0, 'background' + _number)
        BACKGROUND.width = Game.width
        BACKGROUND.height = Game.height
        return BACKGROUND
    },
    createPlayer: function (_scale) {
        const PLAYER = Game.add.sprite(Game.width / 2, Game.height / 2, 'dude')
        PLAYER.anchor.setTo(0.5)
        PLAYER.scale.setTo(_scale)
        PLAYER.animations.add('walk-right', [5, 6, 7, 8], 10, true)
        PLAYER.animations.add('walk-left', [0, 1, 2, 3], 10, true)
        Game.physics.enable(PLAYER)
        return PLAYER
    },
    movePlayer: function (_player, _speed) {
        const cursors = Game.input.keyboard.createCursorKeys()
        if (cursors.left.isDown) {
            _player.body.velocity.x = -_speed
            _player.animations.play('walk-left')
        } else if (cursors.right.isDown) {
            _player.body.velocity.x = _speed
            _player.animations.play('walk-right')
        } else {
            _player.frame = 4
            _player.body.velocity.x = 0
        }

        if (cursors.down.isDown) {
            _player.body.velocity.y = _speed
        } else if (cursors.up.isDown) {
            _player.body.velocity.y = -_speed
        } else {
            _player.body.velocity.y = 0
        }
    }
}