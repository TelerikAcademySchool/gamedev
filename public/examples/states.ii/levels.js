'use strict'
let player
const FirstState = {
    create: function () {
        Functions.createBackgound(1)
        player = Functions.createPlayer(0.7)

        Game.input.onDown.add(function () {
            Game.state.start('second')
        })
    },
    update: function () {
        Functions.movePlayer(player, 200)
    }
}

const SecondState = {
    create: function () {
        Functions.createBackgound(2)
        player = Functions.createPlayer(1)
        Game.input.onDown.add(function () {
            Game.state.start('third')
        })
    },
    update: function () {
        Functions.movePlayer(player, 300)
    }
}

const ThirdState = {
    create: function () {
        Functions.createBackgound(3)
        player = Functions.createPlayer(1.4)

        Game.input.onDown.add(function () {
            Game.state.start('first')
        })
    },
    update: function () {
        Functions.movePlayer(player, 400)
    }
}