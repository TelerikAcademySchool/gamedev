'use strict'
const Preload = {
    preload: function () {
        for (let i = 1; i <= 3; i++) {
            Game.load.image('background' + i, '../assets/images/background-sky (' + i + ').png')
        }
        Game.load.spritesheet('dude', '../assets/images/dude-org.png', 288 / 9, 48)
    },
    create: function () {
        Game.state.start('first')
    }
}
