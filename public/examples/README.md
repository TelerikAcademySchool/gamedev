> You can search by name and see the preview of the example at https://telerikacademyschool.gitlab.io/gamedev/.
>  
> - Each example is a single **.html** file. The only exception are the examples for states.  
> - All the used images and other resources are in the **assets** folder.  
> - Phaser.js v2.12.0 is in the **lib** folder.  
> - Each example contains a **script** tag, which is the actual game. 

## Portfolio

This is the new vertion of Creative Port. The porfolio contains all the study examples, projects and workshops, used in Game Development program by Telerik Academy School.  
**The examples are intended for students and teachers of Telerik Academy School.**

Check the examples at https://telerikacademyschool.gitlab.io/gamedev/.

## Website
Learn more about Telerik Academy School at https://www.telerikacademy.com/school.

## Moodle
If you are already a student, check your course at https://school.telerikacademy.com.
