'use strict'
const Game = new Phaser.Game(600, 400, Phaser.AUTO, 'game-canvas')

Game.state.add('HomeScreen', HomeState)
Game.state.add('FirstLevel', FirstState)
Game.state.add('SecondLevel', SecondState)

Game.state.start('HomeScreen')